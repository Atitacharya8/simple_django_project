from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from blogs.models import Review


class ReviewForm(forms.ModelForm):
    class Meta:
         model = Review
         fields = ["rate","review"]


class UserSignupForm(UserCreationForm):
    class Meta:
        model=User
        fields=["username","email","first_name","last_name","password1","password2"]

class LoginForm(forms.Form):
    username=forms.CharField(max_length=250)
    password=forms.CharField(max_length=250,widget=forms.PasswordInput)



