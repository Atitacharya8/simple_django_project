from django.contrib.auth import login, logout
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate

# Create your views here.
from blogs.forms import ReviewForm, UserSignupForm, LoginForm
from blogs.models import Article


def list(req):
    datas = Article.objects.filter(published=True)
    return render(req, "blogs/index.html", {"datas": datas})

def search(req):
    q=req.GET["q"]
    datas = Article.objects.filter(published=True,title__contains=q)
    return render(req, "blogs/index.html", {"datas": datas})


def detail(req, id):
    form = ReviewForm()
    article = Article.objects.get(id=id)
    article.views += 1
    article.save()
    return render(req, "blogs/detail.html", {"article": article, "form": form})


def review(req, id):
    if req.method == "POST":
        form = ReviewForm(req.POST)
        myreview = form.save(commit=False)
        myreview.article = Article.objects.get(id=id)
        myreview.user = req.user
        myreview.save()

    return redirect("/blogs/%d/" % id)


def mysignup(req):

    if req.method=="POST":
        form=UserSignupForm(req.POST)
        if form.is_valid():
            user=form.save(commit=False)
            #process for initial user
            user.save()
            return redirect("/blogs/")

    else:
        form = UserSignupForm()




    form=UserSignupForm()
    return render(req,"blogs/signup.html",{"form":form})



def mylogin(req):
    # if req.user.is_authenticated:
    #     return redirect("/blogs/")
    if req.method=="POST":
        form=LoginForm(req.POST)
        username=req.POST["username"]
        password=req.POST["password"]

        user=authenticate(username=username,password=password)
        if user:

             #check for user status if user.is
          login(req,user)
          return redirect("/blogs/")
    else:
        form=LoginForm()

    form=LoginForm()
    return render(req,"blogs/login.html",{"form":form})

def mylogout(req):
    logout(req)
    return redirect("/blogs/")

