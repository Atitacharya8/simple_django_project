from django.contrib import admin
from django.urls import path
from django.urls import include

from blogs import views

urlpatterns = [
    path('', views.list),
    path('search/', views.search),
    path("<int:id>/",views.detail),
    path('<int:id>/review/', views.review),
    path('login/', views.mylogin),
    path('logout/', views.mylogout),
    path('signup/', views.mysignup),

]
