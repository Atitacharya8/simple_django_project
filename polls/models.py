from django.db import models


# Create your models here.
class Question(models.Model):
    text = models.CharField(max_length=250)
    created = models.DateTimeField()
    modified = models.DateTimeField()
    views = models.IntegerField(default=0)

    def __str__(self):
        return self.text


class choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    text = models.CharField(max_length=250)
    vote = models.IntegerField(default=0)
    created = models.DateTimeField()

    def __str__(self):
        return self.question.text + " - " + self.text
