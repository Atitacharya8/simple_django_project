from django.contrib import admin

# Register your models here.
from polls import models


class ChoiceInline(admin.TabularInline):
    model = models.choice
    extra = 5
    fields=["text","created","vote"]



class QuestionAdmin(admin.ModelAdmin):
    list_display = ['text', "created", "modified", "views"]
    search_fields = ['text', 'views']
    list_filter = ['created', "views"]
    inlines = [ChoiceInline]
    #fields=["text",'created', 'modified','views']
    fieldsets = [
        ["descrption", {"fields": ["text","views"]}],
        ['date information', {'fields': ['created', 'modified']}]


    ]


admin.site.register(models.Question, QuestionAdmin)
#admin.site.register(models.choice)