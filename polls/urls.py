from django.urls import path

from polls import views

urlpatterns = [

    path('', views.list),
    path('<int:id>/', views.detail),
    path('<int:id>/vote/', views.vote),

]
