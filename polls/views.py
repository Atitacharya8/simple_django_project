from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from polls import models


def home(req):
    return HttpResponse("hello world  this is me atit acharya")


def list(req):
    questions = models.Question.objects.all()
    context = {"questions": questions, "hello": "there"}
    return render(req, "polls/home.html", context)


def detail(req, id):
    question = models.Question.objects.get(id=id)
    question.views += 1
    question.save()
    context = {"question": question}
    return render(req, "polls/detail.html", context)


def vote(req, id):
    print(" question id", id)
    choice_id = req.POST['choice']
    print("selected=choice", choice_id)
    question = models.Question.objects.get(id=id)

    choice = question.choice_set.get(id=choice_id)

    choice.vote += 1
    choice.save()
    return redirect("/polls/" + str(id) + "/")
